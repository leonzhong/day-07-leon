package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    @Resource
    private CompanyRepository companyRepository;


    @GetMapping
    public List<Company> getAllCompanies() {
        return companyRepository.listAll();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Long id) {
        return companyRepository.getById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Long id){
        return companyRepository.getByCompanyId(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> listCompaniesByPage(@RequestParam Integer page, @RequestParam Integer size) {
        return companyRepository.listAll(page, size);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company createCompany(@RequestBody Company company) {
        return companyRepository.add(company);
    }

    @PutMapping("/{id}")
    public Company updateCompanyById(@PathVariable Long id, @RequestBody Company company) {
        return companyRepository.update(id, company);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteCompany(@PathVariable Long id) {
        companyRepository.remove(id);
    }
}
