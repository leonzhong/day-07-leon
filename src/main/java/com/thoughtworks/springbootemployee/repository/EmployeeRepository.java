package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    public final List<Employee> employees = new ArrayList<>();

    private Employee getEmployee(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    private Long generateId() {
        return employees.stream()
                .max(Comparator.comparingLong(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public Employee add(Employee employee) {
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }

    public List<Employee> listAll() {
        return employees;
    }

    public List<Employee> listAll(Integer page, Integer size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }


    public Employee getById(Long id) {
        return getEmployee(id);
    }

    public List<Employee> getByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee update(Long id, Employee employee) {
        Employee updatedEmployee = getEmployee(id);
        if (Objects.nonNull(employee)) {
            updatedEmployee.setSalary(employee.getSalary());
            updatedEmployee.setAge(employee.getAge());
            updatedEmployee.setCompanyId(employee.getCompanyId());
        }
        return updatedEmployee;
    }

    public void remove(Long id) {
        employees.remove(getEmployee(id));
    }

    public void removeByCompanyId(Long companyId) {
        List<Employee> employeeList = employees.stream()
                .filter(employee -> Objects.equals(employee.getCompanyId(), companyId))
                .collect(Collectors.toList());
        employees.removeAll(employeeList);
    }
}
