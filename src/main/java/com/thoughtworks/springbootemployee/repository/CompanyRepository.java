package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    @Resource
    private EmployeeRepository employeeRepository;
    private final List<Company> companies = new ArrayList<>();

    public Company add(Company company) {
        company.setCompanyId(generateId());
        this.companies.add(company);
        return company;
    }

    private Long generateId() {
        return companies.stream()
                .max(Comparator.comparingLong(Company::getCompanyId))
                .map(employee -> employee.getCompanyId() + 1)
                .orElse(1L);
    }

    public List<Company> listAll() {
        return this.companies;
    }

    public List<Company> listAll(Integer page, Integer size) {
        return this.companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company getById(Long id) {
        return getCompany(id);
    }

    private Company getCompany(Long id) {
        return companies.stream()
                .filter(company -> company.getCompanyId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getByCompanyId(Long companyId) {
        return employeeRepository.listAll().stream()
                .filter(employee -> Objects.equals(employee.getCompanyId(), companyId))
                .collect(Collectors.toList());
    }

    public Company update(Long id, Company company) {
        Company updatedCompany = getCompany(id);
        if (Objects.nonNull(company)){
            updatedCompany.setCompanyName(company.getCompanyName());
        }
        return updatedCompany;
    }

    public void remove(Long companyId) {
        this.companies.remove(getCompany(companyId));
        employeeRepository.removeByCompanyId(companyId);
    }

}
