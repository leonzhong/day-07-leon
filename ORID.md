Objective:

- Today we reviewed the concept map of TTD and revisited the concepts of TDD, concept maps, OOP, and refactor.
- In the afternoon, we learned about the SpringBoot framework and how to write SpringBoot code, with a focus on using the framework.

Reflective:

- This is a way for me to consolidate my existing knowledge.

Interpretive:

- During today's learning, we reviewed the concepts of http request, restful api, and the knowledge we learned last week. These are all methods to help us consolidate our knowledge.

Decision:

- I am ready to embrace new challenges and learn SpringBoot.